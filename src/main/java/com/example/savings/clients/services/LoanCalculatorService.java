package com.example.savings.clients.services;

import com.example.savings.clients.entities.ClientSavings;
import com.example.savings.clients.repositories.ClientSavingsRepository;
import com.example.savings.clients.requests.LoanCalculatorRequest;
import com.example.savings.utils.Helpers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class LoanCalculatorService extends Helpers {

    private final ClientSavingsRepository clientSavingsRepository;

    @Autowired
    public LoanCalculatorService(ClientSavingsRepository clientSavingsRepository) {
        this.clientSavingsRepository = clientSavingsRepository;
    }

    public ResponseEntity<Map<String, Object>> calculate(LoanCalculatorRequest request) {
        Map<String, Object> map = new HashMap<>();

        if (request.getAmount().compareTo(BigDecimal.ZERO) <= 0){
            failure(map);
            map.put("message", "Enter a valid amount");
            return ResponseEntity.badRequest().body(map);
        }
        if (request.getPeriod() <= 0 || request.getPeriod() > 36){
            failure(map);
            map.put("message", "Enter a valid period");
            return ResponseEntity.badRequest().body(map);
        }

        Optional<ClientSavings> optionalClientSavings = clientSavingsRepository.findFirstByClientIdOrderByCreatedAtDesc(request.getClientId());
        if (optionalClientSavings.isEmpty() || optionalClientSavings.get().getNewBalance().compareTo(BigDecimal.valueOf(500000)) < 0){
            failure(map);
            map.put("message", "The client does not qualify for a loan");
            return ResponseEntity.badRequest().body(map);
        }

        BigDecimal interest = request.getAmount().multiply(BigDecimal.valueOf(0.14).divide(BigDecimal.valueOf(12), 2, RoundingMode.HALF_UP)).multiply(BigDecimal.valueOf(request.getPeriod())).setScale(2, RoundingMode.UP);

        map.put("loanAmount", request.getAmount());
        map.put("period", request.getPeriod());
        map.put("totalRepaymentAmount", request.getAmount().add(interest));
        map.put("monthlyRepayment", request.getAmount().add(interest).divide(BigDecimal.valueOf(request.getPeriod()), 2, RoundingMode.HALF_UP));

        return ResponseEntity.ok(success(map));
    }
}
