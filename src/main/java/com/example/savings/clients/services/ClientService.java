package com.example.savings.clients.services;

import com.example.savings.clients.entities.Client;
import com.example.savings.clients.repositories.ClientRepository;
import com.example.savings.clients.requests.ClientRequest;
import com.example.savings.utils.Helpers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class ClientService extends Helpers {

    private final ClientRepository clientRepository;

    @Autowired
    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public ResponseEntity<Map<String, Object>> get() {
        Map<String, Object> map = new HashMap<>();

        map.put("data", clientRepository.findAll());

        return ResponseEntity.ok(success(map));
    }

    public ResponseEntity<Map<String, Object>> create(ClientRequest request) {
        Map<String, Object> map = new HashMap<>();

        clientRepository.save(new Client(request.getName()));

        return ResponseEntity.ok(success(map));
    }

    public ResponseEntity<Map<String, Object>> update(Long id, ClientRequest request) {
        Map<String, Object> map = new HashMap<>();

        Optional<Client> optionalClient = clientRepository.findById(id);
        if (optionalClient.isEmpty()){
            return ResponseEntity.badRequest().body(failure(map));
        }
        Client client = optionalClient.get();
        client.setName(request.getName());
        client.setUpdatedAt(LocalDateTime.now());
        clientRepository.save(client);

        return ResponseEntity.ok(success(map));
    }

}
