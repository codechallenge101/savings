package com.example.savings.clients.services;

import com.example.savings.clients.entities.Client;
import com.example.savings.clients.entities.ClientSavings;
import com.example.savings.clients.enums.JournalEntryTypes;
import com.example.savings.clients.repositories.ClientRepository;
import com.example.savings.clients.repositories.ClientSavingsRepository;
import com.example.savings.clients.requests.ClientSavingsRequest;
import com.example.savings.utils.Helpers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class ClientSavingService extends Helpers {

    private final ClientSavingsRepository clientSavingsRepository;
    private final ClientRepository clientRepository;

    @Autowired
    public ClientSavingService(ClientSavingsRepository clientSavingsRepository, ClientRepository clientRepository) {
        this.clientSavingsRepository = clientSavingsRepository;
        this.clientRepository = clientRepository;
    }

    public ResponseEntity<Map<String, Object>> get() {
        Map<String, Object> map = new HashMap<>();

        map.put("data", clientSavingsRepository.findAll());

        return ResponseEntity.ok(success(map));
    }

    public ResponseEntity<Map<String, Object>> getClientSavings(Long clientId) {
        Map<String, Object> map = new HashMap<>();

        Optional<Client> optionalClient = clientRepository.findById(clientId);
        if (optionalClient.isEmpty()){
            return ResponseEntity.badRequest().body(failure(map));
        }

        map.put("data", clientSavingsRepository.findAllByClientIdOrderByCreatedAtDesc(clientId));

        return ResponseEntity.ok(success(map));
    }

    public ResponseEntity<Map<String, Object>> createSaving(ClientSavingsRequest request) {
        Map<String, Object> map = new HashMap<>();

        Optional<Client> optionalClient = clientRepository.findById(request.getClientId());
        if (optionalClient.isEmpty()){
            return ResponseEntity.badRequest().body(failure(map));
        }

        ClientSavings clientSavings = new ClientSavings();
        clientSavings.setClientId(request.getClientId());
        clientSavings.setJournalEntryTypeId(request.getJournalEntryType().getId());
        clientSavings.setAmount(request.getAmount());

        Optional<ClientSavings> optionalClientSavings = clientSavingsRepository.findFirstByClientIdOrderByCreatedAtDesc(request.getClientId());
        if (optionalClientSavings.isPresent()){
            clientSavings.setPreviousBalance(optionalClientSavings.get().getNewBalance());
            clientSavings.setNewBalance(request.getJournalEntryType() == JournalEntryTypes.CREDIT ? clientSavings.getPreviousBalance().add(request.getAmount()) : clientSavings.getPreviousBalance().subtract(request.getAmount()));
        } else {
            clientSavings.setPreviousBalance(BigDecimal.ZERO);
            clientSavings.setNewBalance(request.getJournalEntryType() == JournalEntryTypes.CREDIT ? request.getAmount() : BigDecimal.ZERO.subtract(request.getAmount()));
        }

        if (request.getJournalEntryType() == JournalEntryTypes.DEBIT && request.getAmount().compareTo(clientSavings.getPreviousBalance()) < 0){
            failure(map);
            map.put("message", "Insufficient Balance");
            return ResponseEntity.badRequest().body(map);
        }

        clientSavingsRepository.save(clientSavings);

        return ResponseEntity.ok(success(map));
    }
}
