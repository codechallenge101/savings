package com.example.savings.clients.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "client_savings")
public class ClientSavings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @JsonIgnore
    @Column(name = "client_id")
    private Long clientId;

    @JsonIgnore
    @JoinColumn(name = "client_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private Client client;

    @JsonIgnore
    @Column(name = "journal_entry_type_id")
    private Long journalEntryTypeId;

    @JsonIgnore
    @JoinColumn(name = "journal_entry_type_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private JournalEntryType journalEntryType;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "previous_balance")
    private BigDecimal previousBalance;

    @Column(name = "new_balance")
    private BigDecimal newBalance;

    @Column(name = "created_at")
    private LocalDateTime createdAt = LocalDateTime.now();

    @Column(name = "updated_at")
    private LocalDateTime updatedAt = LocalDateTime.now();

    public String getName(){return client.getName();}

    public String getEntryType(){return journalEntryType.getName();}
}
