package com.example.savings.clients.controllers;

import com.example.savings.clients.requests.ClientSavingsRequest;
import com.example.savings.clients.services.ClientSavingService;
import com.example.savings.utils.Constants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/client-savings")
public class ClientSavingsController {

    private final ClientSavingService clientSavingService;

    @Autowired
    public ClientSavingsController(ClientSavingService clientSavingService) {
        this.clientSavingService = clientSavingService;
    }

    @ApiOperation(value = "Get All Client Savings", notes = "Fetch all client savings")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = Constants.SUCCESS_MESSAGE),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @GetMapping()
    public ResponseEntity<Map<String, Object>> get() {
        return clientSavingService.get();
    }

    @ApiOperation(value = "Get All Savings for Client", notes = "Fetch all client savings")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = Constants.SUCCESS_MESSAGE),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @GetMapping("/{clientId}")
    public ResponseEntity<Map<String, Object>> get(@PathVariable Long clientId) {
        return clientSavingService.getClientSavings(clientId);
    }

    @ApiOperation(value = "Deposit or Withdraw", notes = "Deposit or withdraw from client savings")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = Constants.SUCCESS_MESSAGE),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @PostMapping()
    public ResponseEntity<Map<String, Object>> createSaving(@RequestBody ClientSavingsRequest request) {
        return clientSavingService.createSaving(request);
    }

}
