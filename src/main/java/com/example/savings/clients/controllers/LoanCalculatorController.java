package com.example.savings.clients.controllers;

import com.example.savings.clients.requests.ClientSavingsRequest;
import com.example.savings.clients.requests.LoanCalculatorRequest;
import com.example.savings.clients.services.LoanCalculatorService;
import com.example.savings.utils.Constants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/loan-calculator")
public class LoanCalculatorController {

    private final LoanCalculatorService loanCalculatorService;

    @Autowired
    public LoanCalculatorController(LoanCalculatorService loanCalculatorService) {
        this.loanCalculatorService = loanCalculatorService;
    }

    @ApiOperation(value = "Calculate Loan", notes = "Calculate loan")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = Constants.SUCCESS_MESSAGE),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @PostMapping()
    public ResponseEntity<Map<String, Object>> calculate(@RequestBody LoanCalculatorRequest request) {
        return loanCalculatorService.calculate(request);
    }
}
