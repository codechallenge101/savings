package com.example.savings.clients.controllers;

import com.example.savings.clients.requests.ClientRequest;
import com.example.savings.clients.services.ClientService;
import com.example.savings.utils.Constants;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/clients")
public class ClientController {

    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @ApiOperation(value = "Get Clients", notes = "Fetch all clients")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = Constants.SUCCESS_MESSAGE),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @GetMapping()
    public ResponseEntity<Map<String, Object>> get() {
        return clientService.get();
    }

    @ApiOperation(value = "Create Client", notes = "Create clients")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = Constants.SUCCESS_MESSAGE),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @PostMapping()
    public ResponseEntity<Map<String, Object>> create(@RequestBody ClientRequest request) {
        return clientService.create(request);
    }

    @ApiOperation(value = "Update Client", notes = "update clients")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = Constants.SUCCESS_MESSAGE),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @PutMapping("/{id}")
    public ResponseEntity<Map<String, Object>> update(@PathVariable Long id, @RequestBody ClientRequest request) {
        return clientService.update(id, request);
    }


}
