package com.example.savings.clients.repositories;

import com.example.savings.clients.entities.ClientSavings;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientSavingsRepository extends CrudRepository<ClientSavings, Long> {

    List<ClientSavings> findAllByClientIdOrderByCreatedAtDesc(Long clientId);

    Optional<ClientSavings> findFirstByClientIdOrderByCreatedAtDesc(Long clientId);
}
