package com.example.savings.clients.enums;

public enum JournalEntryTypes {
    DEBIT(1L), CREDIT(2L);

    private final Long id;

    JournalEntryTypes(Long id){
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
