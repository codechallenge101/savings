package com.example.savings.clients.requests;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class LoanCalculatorRequest {
    private Long clientId;
    private BigDecimal amount;
    private Integer period;
}
