package com.example.savings.clients.requests;

import lombok.Data;

@Data
public class ClientRequest {
    private String name;
}
