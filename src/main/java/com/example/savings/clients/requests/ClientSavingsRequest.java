package com.example.savings.clients.requests;

import com.example.savings.clients.enums.JournalEntryTypes;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ClientSavingsRequest {
    private Long clientId;
    private BigDecimal amount;
    private JournalEntryTypes journalEntryType;
}
