package com.example.savings.utils;

public class Constants {
    public static final String SUCCESS_MESSAGE = "Request Processed Successfully";
    public static final String FAIL_MESSAGE = "Failed To Process Request";
    public static final String UNAUTHORIZED = "Unauthorized";
    public static final String SUCCESS_STATUS = "00";
    public static final String FAILURE_STATUS = "01";
}