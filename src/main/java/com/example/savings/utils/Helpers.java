package com.example.savings.utils;

import org.springframework.stereotype.Component;
import java.util.Map;

@Component
@SuppressWarnings("unchecked")
public class Helpers {

    /**
     * Add success status and message to a map
     *
     * @param map Map<String, Object>
     * @return Map<String, Object>
     */
    public static Map<String, Object> success(Map<String, Object> map) {
        map.put("message", Constants.SUCCESS_MESSAGE);
        map.put("status", Constants.SUCCESS_STATUS);
        return map;
    }

    /**
     * Add failure status and message to a map
     *
     * @param map Map<String, Object>
     * @return Map<String, Object>
     */
    public static Map<String, Object> failure(Map<String, Object> map) {
        map.put("message", Constants.FAIL_MESSAGE);
        map.put("status", Constants.FAILURE_STATUS);
        return map;
    }

    /**
     * Unauthorized
     *
     * @param map Map<String, Object>
     * @return Map<String, Object>
     */
    protected static Map<String, Object> unauthorized(Map<String, Object> map) {
        map.put("message", Constants.UNAUTHORIZED);
        map.put("status", Constants.FAILURE_STATUS);
        return map;
    }
}
