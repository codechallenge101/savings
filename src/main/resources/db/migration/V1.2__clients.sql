CREATE TABLE public.clients
(
    id                  BIGINT PRIMARY KEY,
    name                VARCHAR (255),
    created_at          TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at          TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE SEQUENCE public.clients_id_seq
    START WITH 1
    INCREMENT BY 1
    MINVALUE 1
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.clients_id_seq OWNED BY public.clients.id;

ALTER TABLE ONLY public.clients
    ALTER COLUMN id SET DEFAULT nextval('public.clients_id_seq'::regclass);


-- Journal Entry types

CREATE TABLE public.journal_entry_types
(
    id                  BIGINT PRIMARY KEY,
    name                VARCHAR (50)
);

INSERT INTO public.journal_entry_types (id, name) VALUES
(1, 'Debit'),
(2, 'Credit');


-- Client Savings

CREATE TABLE public.client_savings
(
    id                      BIGINT PRIMARY KEY,
    client_id               BIGINT REFERENCES public.clients(id),
    journal_entry_type_id   BIGINT REFERENCES public.journal_entry_types(id),
    amount                  DECIMAL(30, 2) NOT NULL,
    previous_balance        DECIMAL(30, 2) NOT NULL,
    new_balance             DECIMAL(30, 2) NOT NULL,
    created_at              TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at              TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE SEQUENCE public.client_savings_id_seq
    START WITH 1
    INCREMENT BY 1
    MINVALUE 1
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.client_savings_id_seq OWNED BY public.client_savings.id;

ALTER TABLE ONLY public.client_savings
    ALTER COLUMN id SET DEFAULT nextval('public.client_savings_id_seq'::regclass);